import java.util.Scanner;

public class SwapVariables {

	public static void main(String[] args) {

		/*int a = 5;
		int b = 7;
		int temp;
		
		System.out.println("Values before swapping--");
		System.out.println("a:" + a + " b:" + b);
		//Swapping a and b values
	    temp = a ;
	    a = b;
	    b = temp;
	    System.out.println("Values after swapping--");
	    System.out.println("a:"+a + " b:" +b);*/


		int a,b,temp;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter first value:");
		a=s.nextInt();
		
		Scanner s1=new Scanner(System.in);
		System.out.println("Enter second value:");
		b=s1.nextInt();
		
		System.out.println("Value of a:"+a+" Value of b:"+b+" before swapping");
		
		temp = a;
		a=b;
		b=a;
		
		System.out.println("Value of a:"+a+" Value of b:"+b+" after swapping");
		
	}

}
