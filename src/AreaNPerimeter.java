import java.util.Scanner;

public class AreaNPerimeter {

	public static void main(String[] args) {

		//int radius = 5;
		
		//Take input from user for radius of circle
		int radius; //create a variable int 
		Scanner s= new Scanner(System.in); //create object 's' for class Scanner
		System.out.println("Enter radius of circle: "); //Takes input from user and saves in object s
		radius = s.nextInt();  //Takes input and converts to int and saves in 'radius' variable
		
	    double area = Math.PI * radius * radius;
	    System.out.println("Area of circle is: " + area);
	    
	    double perimeter = 2 * Math.PI * radius;
	    System.out.println("Perimeter of circle is: " + perimeter);

	}

}
