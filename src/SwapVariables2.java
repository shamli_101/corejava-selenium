import java.util.Scanner;

public class SwapVariables2 {

	public static void main(String[] args) {
		//Swapping of variables without third variable
		
		/*int a =5;
		int b =7;
		
		System.out.println("Value of a:"+a+" Value of b:"+b+" before swapping");
		
		a=a+b; 
		b=a-b;
	    a=a-b;
	    System.out.println("Value of a:"+a+" Value of b:"+b+" after swapping");*/
		
		int a,b;
		
		Scanner s1=new Scanner(System.in);
		System.out.println("Enter first variable");
		a=s1.nextInt();
		
		Scanner s2=new Scanner(System.in);
		System.out.println("Enter second variable");
		b=s2.nextInt();
		
		System.out.println("Value of a: "+a+" Value of b: " +b+" before swapping");
		
		a=a+b;
		b=a-b;
		a=a-b;
		
		System.out.println("Value of a: "+a+" Value of b: " +b+" before swapping");

	}

}
