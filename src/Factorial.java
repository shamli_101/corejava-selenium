import java.util.Scanner;
public class Factorial {

	public static void main(String[] args) {
		//to get Factorial
		/*The factorial of a number is the product of all the integers from 1 to that number. 
		 For example, the factorial of 6 (denoted as 6!) is 1*2*3*4*5*6 = 720.

		Factorial is not defined for negative numbers and the factorial of zero is one, 0! = 1.*/
	
	   //Find factorial of 3
		
		/*int num = 3;
		int factorial = 1;
		if (num < 0) {
			System.out.println("The factorial of negative number does not exist!!");
		} else if (num == 0) {
			System.out.println("The factorial of 0 is 1.");
		} else {
			 for (int i=1 ; i <=num ; i++) {
				 factorial = factorial * i;
		}
		  System.out.println("Factorial of "+num+" is "+factorial);
			 
	}*/
		
		int num;
		int factorial = 1;
		
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number you want to find factorial for : ");
		num=s.nextInt();
		
		if(num < 0) {
			System.out.println("Factorial of negative number does not exist!!");
		} else if (num == 0) {
			System.out.println("Factorial of 0 is 1!");
		} else {
			for (int i=1 ; i <= num ; i++) {
				factorial = factorial * i;
			}
			System.out.println("Factorial of "+num+" is "+factorial);
		}
		
		
		
}
}