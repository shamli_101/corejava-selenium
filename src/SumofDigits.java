import java.util.Scanner;
public class SumofDigits {

	public static void main(String[] args) {
		//sum of digits of an integer
		/*
		int num = 15;
		int sum = 0, reminder;
		
		while(num > 0)
        {
            reminder = num % 10;
            sum = sum + reminder;
            num = num / 10;
        }
        System.out.println("Sum of Digits:"+sum);*/
		
		int sum = 0, reminder, num;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter an integer");
		num=s.nextInt();
		
		while (num > 0) {
			reminder = num % 10;
			sum = sum + reminder;
			num = num / 10;
		}
		System.out.println("Sum of digits of an integer is "+ sum);
    }
	
	}

